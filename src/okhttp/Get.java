package okhttp;

import java.io.IOException;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class Get {
    OkHttpClient client = new OkHttpClient();

    String run(String url) throws IOException {
        Request request = new Request.Builder()
            .url(url)
            .build();

        Response response = client.newCall(request).execute();
        System.out.println("Code : " + response.code());
        return response.body().string();
    }

    public static void main(String[] args) throws IOException {
        Get example = new Get();
        String response = example.run("http://192.168.0.19:8080/group.service");
        System.out.println(response);
    }
} 

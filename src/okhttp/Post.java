package okhttp;

import java.io.IOException;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class Post {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

  OkHttpClient client = new OkHttpClient();

  String post(String url, String json) throws IOException {
    RequestBody body = RequestBody.create(JSON, json);
    Request request = new Request.Builder()
        .url(url)
        .post(body)
        .build();
    Response response = client.newCall(request).execute();
    System.out.println("Code : " + response.code());

    return response.body().string();
  }

  String PostExample() {
    return "{\"group\":\"model\","
                + "\"description\":\"Message for model\""
                + "}";
  }

  public static void main(String[] args) throws IOException {
    Post example = new Post();
    String json = example.PostExample();
    String response = example.post("http://192.168.0.19:8080/group.service", json);
    System.out.println(response);
  }
}
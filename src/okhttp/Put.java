package okhttp;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.IOException;

public class Put {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

  String put(String url, String json) throws IOException {
    RequestBody body = RequestBody.create(JSON, json);
    Request request = new Request.Builder()
        .url(url)
        .put(body)
        .build();
    Response response = client.newCall(request).execute();
    System.out.println("Code : " + response.code());

    return response.body().string();
  }

  String PutExample() {
    return "{\"group\":\"model\","
                + "\"description\":\"Hello World\""
                + "}";
  }

  public static void main(String[] args) throws IOException {
    Put example = new Put();
    String json = example.PutExample();
    String response = example.put("http://192.168.0.19:8080/group.service", json);
    System.out.println(response);
  }
}
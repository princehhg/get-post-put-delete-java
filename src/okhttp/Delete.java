package okhttp;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.IOException;

public class Delete {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    
    OkHttpClient client = new OkHttpClient();

  String delete(String url, String json) throws IOException {
    RequestBody body = RequestBody.create(JSON, json);
    Request request = new Request.Builder()
        .url(url)
        .delete(body)
        .build();
    
    Response response = client.newCall(request).execute();
    System.out.println("Code : " + response.code());
    return response.body().string();
  }

  String DeleteExample() {
    return "{\"group\":\"model\","
            + "\"description\":\"\""
            + "}";
  }

  public static void main(String[] args) throws IOException {
    Delete example = new Delete();
    String json = example.DeleteExample();
    String response = example.delete("http://192.168.0.19:8080/group.service", json);
    System.out.println(response);
  }
}